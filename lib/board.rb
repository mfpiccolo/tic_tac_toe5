class Board

  def initialize
    @board = Array.new(3) {Array.new(3) {' '}}
  end

  def full?
    false
  end

  def winner?
    @board.each do |row|
      if row[1] == row[0] && row[1] == row[2] then return row[1] end
    end
    @board.transpose.each do |row|
      if row[1] == row[0] && row[1] == row[2] then return row[1] end
    end
    if (@board[1][1] == @board[0][0] && @board[1][1] == @board[2][2]) ||
       (@board[1][1] == @board[0][2] && @board[1][1] == @board[2][0])
      return row[1][1]
    end
    return nil
  end

  def set_mark(row, column, player)
    @board[row-1][column-1] = player.mark
  end

  def to_s
    puts "............"
    @board.each do |row|
      print "|"
      row.each do |mark|
        print " #{mark} |"
      end
      puts "\n............"
    end
  end

end


