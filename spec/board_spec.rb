require '../lib/board'
require '../lib/player'

board = Board.new
player = Player.new('X')
puts "#{board} is an instance of Board"

puts "#{board.full?} should be false"
puts "#{board.winner?} should be nil"
p "#{board} should be an empty board"

board.set_mark(1, 1, player)
board.set_mark(1, 2, player)
board.set_mark(1, 3, player)
print "#{board} should be an board with an X in the center\n"


print "#{board.winner?} should be 'X'"

